<?php
/**
 * @file transport.remote_entity.inc
 * Contains the transport remote entity class, subclasses, and interfaces.
 */

/**
 * The base class to handle a remote entity.
 *
 * The lifecycle of a remote entity runs through several phases.
 * Each phase is run with a standard method, of the form
 * run_phase_PHASENAME.
 * Each phase may choose to hand over control to the next phase or halt the 
 * entire processing of the entity (and thus return to the controller) 
 * -- currently only the dependencies phase does this.
 *
 * We faces.module's extender faces pattern to allow phase methods to be
 * defined in any of:
 *  - this base class
 *  - a child class for the entity type. 
 *    Class names are of the form TransportRemoteEntity_TYPE.
 *  - An interface loaded by the chid class. This particular pattern allows
 *    us to define a phase method for any combination of entity type and
 *    source type. This is what allows different ways to fetch remote data
 *    depending on entity type and source type (eg, getting node data from
 *    a Drupal 5 site, getting user data from a Drupal 6 site, etc).
 *
 * The phases are as follows:
 *
 * - object construction (not really a phase)
 *   - The object is created. Its class reflects the type of entity.
 *   - The object is extended further according to source and type.
 *     TODO: create a system allowing applications to request a subset
 *     of phases, or to add extra phases.
 *   - The local entity is identified, if one exists.
 *   - The stash is checked: if we already fetched this object once in this set,
 *     we are free to skip the fetch phase.
 *     TODO: circular dependencies -- need to think about how to handle these.
 * 
 * - TODO: at this point we should check to see if the entity has been previously 
 *   retrieved within a set timeframe, eg if the node was retrieved less than a day ago
 *   consider it to still be fresh and skip it.
 *   Freshness should be per-type; eg you might want to say that taxonomy terms
 *   never need to be retrieved again and should always be skipped.
 * 
 * 'fetch'
 *    - The data for the entity is fetched from the source.
 *    - The entity is compared with the local version of the entity, if it exists.
 *    - TODO: If this is more recent than the required freshness of the controller operation,
 *      the entity is skipped; control returns to the Controller with a status to move on to the next entity.
 * 
 * 'filter'
 *   The entity is run through:
 *     1. Custom filters provided by drupal_alter hooks.
 *       This is where you can opt to remove properties that are not relevant to the local site, or
 *       massage data. 
 *     2. The filter provided by its type, according to source type.
 *       Eg, taxo terms from Drupal 5 automatically get tidied up for the 
 *       current local version of Drupal.
 * 
 * 'dependencies'
 *   The entity is run through a hook that allows all modules to add to a list
 *   of entity dependencies. This is a flat list, eg term 35, node 4, user 3.
 *   At this point, if there are dependencies, execution of the entity handler
 *   stops and control goes back to the controller object. The controller
 *   processes the dependent entities first, and then resumes this entity's
 *   execution at its next phase.
 *   
 * 'translate'   
 *   References in the entity to remote entities are translated to local IDs.
 *   Eg, a node's author is set to a local user (who has been retrieved as a
 *   dependency).
 *
 * 'preexisting' -- TODO
 *   If the entity has not previously been retrieved, check whether an entity
 *   exists locally which matches it anyway. Eg, check for a local user with
 *   the same email address.
 * 
 * 'save'
 *   The entity is saved.
 *
 * 'record'
 *   The correspondence between remote ID and local ID for this particular
 *   source is stored in the database.
 */
class TransportRemoteEntity extends FacesExtendable {
  // These pretty much all have to be public because objects of this class
  // need to be serialized to the database. Bang goes our privacy :(
  // TODO: phases should get set by the constructor so 
  // more that defined here can be provided.
  // ALSO: need to distinguish phases that take an interface and those that don't!
  // Some phases are commented out and TODO, as I don't need them for this current project.
  public $phases = array(
    // init,
    'fetch',
    'filter',
    'dependencies',
    'translate',
    // 'preexisting', // TODO
    'save',
    'record'
  );
  // Phases which need to load interfaces. 
  // Graaargh I don't like the partial duplication with the above $phases, but
  // the alternative is a more complex array. One to ponder. TODO
  // In fact, consider naming faces that are not phase names,
  // as we only need one face in the core module at least.
  // Hence this one face could be called 'core', or 'source' since it
  // diverges on source type.
  public $faced_phases = array(
    'fetch', 
    //'filter',
  );
  public $current_phase;   // The phase currently being run or just completed.
  public $phase_checklist; // This array of phases is emptied as each one is run.
  
  // Data about the entity this object handles.
  public $type;
  public $id_remote;
  public $entity; // This holds the data we fetch from remote
    // and is gradually manipulated over the lifecycle.
  
  // Data about the local entity.
  public $local_exists = FALSE; // Flag whether we have imported in a previous set. Does not change.
    // If this is TRUE, the $this->id_local also is set.
  public $prior_exists = FALSE; // Flag whether this object exists but has not been retrieved by us.
    // Example: a user with the same email address.
  public $local_id; // The local id of the entity. Set as soon as the entity is saved.

  // Other data that gets saved to the stash.
  public $base_time;
  
  /**
   * Constructor method.
   *
   * Set properties for the object and loads interfaces.
   */
  function __construct($controller, $entity_data) {
    // Set our controller and copy some data from it.
    $this->controller   = $controller;
    $this->source_name  = $controller->source_name;
    $this->source       = $controller->source;
    $this->base_time    = $controller->get_base_time();
    
    //dsm("basetime: $this->base_time");
    
    // Set our basic parameters from what we were passed in.
    $this->data       = $entity_data;
    $this->type       = $entity_data['type'];
    $this->id_remote  = $entity_data['id']; // BAD NAME. CHANGE.
    $this->id_local;
    
    // Extend the class with interfaces.
    // For each *requested* phase (API TODO!), extend 

    //dsm($this->controller->remote_entity_classes);
    $source_class = get_class($this->source);
  
    // Load the interface for each phase.
    foreach ($this->faced_phases as $phase) {
      //dsm($phase);
      //dsm($this->controller->remote_entity_classes[$this->type]['phases']);
      $interface = 'TransportRemoteEntityInterface_' . $phase;
      $extender_class = $this->controller->remote_entity_classes[$this->type]['phases'][$phase][$source_class];
      //dsm($interface_class);
      $this->extendByClass(array($interface), $extender_class);
    }
    
    // Make a copy of the phase list that we can shift off to keep track of what's next.
    $this->phase_checklist = $this->phases;
    
    // Mark that we're new so drupal_write_record() can be told later
    // when stashing the object between phases.
    $this->new = TRUE;
    
    // Find out if this entity has previously been retrieved.
    $this->get_local_entity();
  }
  
  /**
   * Due to a WTF in Faces we have to implement this ourselves :/
   */
  public function __sleep() {
    $array = get_object_vars($this);
    return array_keys($array);
  }  
  
  /**
   * Accessor functions.
   */
  function get_id() {
    return $this->id_remote;
  }
  
  /**
   * Get the local id of the remote entity we represent, in the event that
   * it's previously been retrieved.
   */
  function get_local_entity() {
    //dpm("get local $this->source_name, $this->source_id, $this->type");
    $result = db_query("SELECT id FROM {transport_entity_remote} WHERE source = '%s' AND id_remote = %d AND type = '%s'",
      $this->source_name, $this->id_remote, $this->type);
      
    $local_entity_id = (int) db_result($result);
    if ($local_entity_id) {
      $this->id_local     = $local_entity_id;
      $this->local_exists = TRUE;
      //dpm("local exists: $this->type $local_entity_id");
    }
  } 
  
  /**
   * Run the object through as many phases as possible.
   *
   * The controller object calls this. Operation is handed back to it either
   * when the entity is completely retrieved, or it encounters a problem or
   * a dependency to resolve.
   */
  function run() {
    // Loop through phases.
    // Each phase has an entry point function run_phase_FOO.
    // This should set the property $this->advance_phase to:
    //  TRUE if the phase is complete
    //  FALSE otherwise,
    // in which case we need to look into the status of the object for more details.
    $this->advance_phase = TRUE;
    //dsm($this->phase_checklist);
    while ($this->advance_phase && count($this->phase_checklist)) {
      
      $this->current_phase = array_shift($this->phase_checklist);
      $this->phase = $this->current_phase; // ugly :(

      // Run the current phase.
      $phase_function = 'run_phase_' . $this->current_phase;
      //dsm("phase $this->current_phase -- function: $phase_function");
      $this->$phase_function();
      
      //$this->advance_phase = FALSE;
      
      // For now, just stash at the end of every phase.
      // TODO: not all phases require stashing; really only those that may halt operation.
      $this->object = $this;
      if (isset($this->new)) {
        drupal_write_record('transport_processing_entities', $this);
        unset($this->new);
      }
      else {
        drupal_write_record('transport_processing_entities', $this, array('base_time', 'type', 'id_remote'));
      }
      unset($this->object);
    } // while loop.
    
    // Figure out why we've stopped executing phases:
    // either we're done, or a phase has set $this->advance_phase to FALSE
    // and we're stalled.
    if ($this->advance_phase) {
      $this->complete = TRUE;
    }
    else {
      $this->stalled = TRUE;
      // TODO: this'll need clearing if ever more than one phase may stall!
    }
  }
  
  /**
   * Phase handler: dependencies.
   *
   * This should be common to all entities.   
   *
   * Now the object has been filtered we don't need to worry about versions of
   * Drupal: assume it's in a format that could be saved directly.
   *
   * However we may need other objects to exist locally (and have IDs) before
   * we save the current one.
   *
   * This phase allows modules to add dependencies.
   *
   * TODO: add a system to allow the application layer to skip some or
   * all dependencies. Using the filter phase to just delete keys won't
   * cut it, as those may need to be saved anyway.
   * Use case is migration where the order of operations can be fixed in 
   * a logical order.
   * Idea: set a $dep_skip_keys array in the initial $options,
   * and in this phase, stash those keys before invoking the hook,
   * then put them back straight after.
   */  
  function run_phase_dependencies() {
    // Invoke hook_transport_process_dependencies_TYPE() in all modules to
    // let them process the entity and add to its dependency list
    $dependencies = module_invoke_all('transport_process_dependencies_' . $this->type, $this->entity);
    
    // Add in some data that we can save the hook implementations the bother of.
    foreach ($dependencies as $i => $dep) {
      $dependencies[$i]['class'] = 'TransportRemoteEntity_' . $dep['type'];
    }
    
    // Now we have all dependencies.
    $this->dependencies = $dependencies;

    // If there are dependencies, stop the process on this entity;
    // return to the controller object.
    if (count($this->dependencies)) {
      $this->advance_phase = FALSE;
    }
  }

  /**
   * Phase handler: record
   *
   * This should be common to all entities.
   *
   * TODO: only record if something was saved!!!!!
   */
  function run_phase_record() {  
    // Only record if no local id.
    $record = array(
      'source'    => $this->source_name,
      'id_remote' => $this->id_remote,
      'type'      => $this->type,
      'base_time' => $this->base_time,
      'id'        => $this->id_local,
    );
    
    //dsm("recording $this->type $this->id_remote locally as  $this->id_local");
    
    // Test on local_exists, as the id_local will always be set by this point.
    // local_exists represents the state on initialization and does not
    // subsequently change.
    if ($this->local_exists) {
      drupal_write_record('transport_entity_remote', $record, array('source', 'type', 'id_remote'));
    }
    else {
      drupal_write_record('transport_entity_remote', $record);
    }
  }
}

// ======================================== Phase interfaces

/**
 * Some phases the remote entity can go through are handled by an interface.
 
 A thought -- do we really need interfaces for EVERY phase?
 how about just one interface for stuff that is source-specific??
 Let's try sticking it all in the 'fetch' interface until we hit trouble... 
 then if it works, just rename ;)
*/

/**
 * Interface for fetch phase.
 */
interface TransportRemoteEntityInterface_fetch {
  function run_phase_fetch();
  function run_phase_filter();
}

/**
 * Common class for fetch phase. Provides helper functions.
 */
abstract class TransportRemoteEntityExtender_fetch 
  extends FacesExtender 
  implements TransportRemoteEntityInterface_fetch {
}

/**
 * Common class for fetch phase on Client sources. Provides helper functions.
 */
abstract class TransportRemoteEntityExtender_fetch_client 
  extends TransportRemoteEntityExtender_fetch
  implements TransportRemoteEntityInterface_fetch {
  
  /**
   * Check the result of a client XMLRPC method call and handle errors.
   *
   * @return
   *  TRUE if the result is successful; FALSE otherwise.
   */
  function fetch_verify_client_result($result) {
    // TODO: write me!
    return TRUE;
  }
}

// ======================================== Extender classes: Node

/**
 * Entity class for nodes.
 *
 * The entity class should contain methods for phases that do not have interfaces;
 * ie those that are identical no matter the source.
 */
class TransportRemoteEntity_node extends TransportRemoteEntity {
  /**
   * Phase handler: node - translate.
   */  
  function run_phase_translate() {
    module_invoke_all('transport_process_translate_node', $this);
  }

  /**
   * Phase handler: node - save.
   */  
  function run_phase_save() {
    // If the node exists locally, set the nid and vid so node_save() performs an update.
    if ($this->local_exists) {
      dsm("node exist LOCALLY - $this->id_local");
      $node_local = node_load($this->id_local);
      
      dsm($node_local, '$node_local');
      
      $this->entity->nid = $node_local->nid;
      $this->entity->vid = $node_local->vid;
    }
    
    node_save($this->entity);
    $this->id_local = $this->entity->nid;
  }
}

/**
 * Extender Class: fetch phase.
 *
 * Type:   Node.
 * Source: Clients Drupal 5
 */
class TransportRemoteEntityExtender_fetch_node_d5 
  extends TransportRemoteEntityExtender_fetch_client 
  implements TransportRemoteEntityInterface_fetch {

  /**
   * Phase handler: node - fetch.
   *
   * The fetch phase retrieves data from the source and stores it in the entity object.
   */
  function run_phase_fetch() {
    // Cast to int for fussiness in XMLRPC.
    $nid = (int) $this->object->get_id();
    //dsm($nid);
    //dsm($this->object);
    //$result = $drupal_5->callMethod('user.load', 1);
    // Long chain of objects: because we're in a face, we want the object;
    // and its source is a Clients object on which we call the method to call a method ;)
    $result = $this->object->source->callMethod('node.load', $nid);
    if ($this->fetch_verify_client_result($result)) {
      $this->object->entity = (object) $result;
    }
    else {
      // probably flag a problem and end the phase and return operation to the controller
    }
    // Check for sanity here?
    //dsm($result, 'fetch result');
    //return "node from drupal! $nid";
  }
  
  /**
   * Phase handler: node - filter.
   *
   * The filter phase performs any clean-up required according to the object's version.
   */
  function run_phase_filter() {    
    // It's up to the individual implementation to translate any field names.
    // For instance, if your D5 site has an image in field_foo
    // and you want this to go into foo_bar, you need to move things
    // on the object using the alter hook:
    drupal_alter('transport_preprocess_' . $this->object->type, $this->object->entity);

    // Actually very little needs to be done here usually.
    // Unset the nid and fid.
    unset($this->object->entity->nid);
    unset($this->object->entity->vid);
    
    
    // DEVELOPMENT CODE!!!!! REMOVE THIS LATER TODO TODO TODO
    unset($this->object->entity->taxonomy);
  }
}

// ======================================== Stand-in hook implementations: node
/**
 * Implementation of hook_transport_process_dependencies_node() for user module.
 *
 * Verifies a node for user dependencies from the author of the node.
 *
 * hook_transport_process_dependencies_node() should return an array of
 * dependencies of the given entity, in the form:
 * $dependencies[] = array(
 *  'type' => TYPE,
 *  'id'   => ID,
 * );
 */
function user_transport_process_dependencies_node($remote_entity) {
  // The author of the node is a user dependency.
  // If you don't want to bring over users, use the alter hook in the filter phase
  // to change the uid of the node to, say, 1.
  //dsm($remote_entity, 'user deps');
  $deps[] = array(
    'type'  => 'user',
    'id'    => $remote_entity->uid,
  );
  return $deps;
}

/**
 * Implementation of hook_transport_process_dependencies_node() for taxonomy.
 */
function taxonomy_transport_process_dependencies_node($remote_entity) {
  // TODO
}

/**
 * Implementation of hook_transport_process_dependencies_node() for upload.
 *
 * Verifies a node for file dependencies from upload.
 */
function upload_transport_process_dependencies_node($remote_entity) {
  // TODO.
}

/**
 * Implementation of hook_transport_process_dependencies_node() for filefield.
 *
 * Verifies a node for file dependencies in CCK file- and imagefields.
 */
function filefield_transport_process_dependencies_node($remote_entity) {
  // TODO: static cache all this!
  // Get CCK's data about the local node type and thence a list of its fields.
  $content_type_info = content_types($remote_entity->type);
  //dsm($content_type_info);
	$fields = $content_type_info['fields'];
  foreach ($fields as $field_name => $field) {
    if ($field['type'] == 'filefield') {
      $filefields[$field_name] = $field;
    }
  }
  
  // Now we have our data!

  //dsm($remote_entity);
	
	foreach ($filefields as $field_name => $field) {
	  foreach ($remote_entity->$field_name as $delta => $data) {
	    $deps[] = array(
	      'type' => 'file',
        'id'   => $data['fid'],
        // we cheat here for the sake of expediency on D5...
        'data' => array(
          'file'      => $data, 
          'filefield' => $field,
        ),
      );
    }
	}
	
	//dsm($deps, 'file deps');
	return $deps;  
}

/**
 * Implementation of hook_transport_process_translate_node() for user.
 */
function user_transport_process_translate_node($entity_handler) {
  $remote_entity = $entity_handler->entity;

  // WARNING for intrepid debuggers:
  // REALLY BAD THINGS happen to a node if the uid it holds does not
  // correspond to a user in the {users} table!
  // TODO: perhaps some error reporting here?
  $uid_local  = transport_get_local_id($entity_handler->source_name, 'user', $remote_entity->uid);
  $remote_entity->uid = $uid_local;
}

/**
 * Implementation of hook_transport_process_translate_node() for filefield.
 *
 * Grahhhh this is practically the same code as the dependencies phase!!!
 * TODO: figure out a way to be better.
 */
function filefield_transport_process_translate_node($entity_handler) {
  $remote_entity = $entity_handler->entity;

  // TODO: static cache all this!
  // Get CCK's data about the local node type and thence a list of its fields.
  $content_type_info = content_types($remote_entity->type);
  //dsm($content_type_info);
	$fields = $content_type_info['fields'];
  foreach ($fields as $field_name => $field) {
    if ($field['type'] == 'filefield') {
      $filefields[$field_name] = $field;
    }
  }
  
  // Now we have our data!

  dsm($remote_entity, 'tp ff');
	foreach ($filefields as $field_name => $field) {
	  foreach ($remote_entity->$field_name as $delta => $data) {
	    dsm($remote_entity->{$field_name}[$delta]);
	    //$remote_entity->{$field_name}[$delta]
	    // nryugg the node still has the old filepath, of course.
	    // ARGH uprpc had this easier. ina way. total mess too.
	    
	    // We should probably reach into the processed entity list and find it...
	    // or load the file record the same way filefield would?
	    
	    $fid_remote = $data['fid'];
	    //dsm($entity_handler, 'hh');
	    //$file_local = transport_get_stashed_entity($entity_handler->base_time, 'file', $fid_remote);
	    // shit we need source!!
	    $fid_local  = transport_get_local_id($entity_handler->source_name, 'file', $fid_remote);
	    
	    //dsm($fid_local);
	    
	    $remote_entity->{$field_name}[$delta] = array(
	      'fid' => $fid_local,
	    );
    }
	}
}

// ======================================== Extender classes: File

/**
 * Entity class for files.
 *
 * The entity class should contain methods for phases that do not have interfaces;
 * ie those that are identical no matter the source.
 */
class TransportRemoteEntity_file extends TransportRemoteEntity {
  /**
   * Phase handler for 'dependencies'.
   *
   * Files don't have dependencies, at least not on D5.
   * Arg that means we may need to version this too????
   */  
  function run_phase_dependencies() {
  }
  
  /**
   * Phase handler: file - translate.
   */  
  function run_phase_translate() {
    // Nothing to do: no dependencies. 
  }
  
  /**
   * Phase handler: file - save.
   *
   * Oh argh. This may have to be versioned for files :(
   * Which means phases are versioned inconsistently per type... fun!
   */  
  function run_phase_save() {
    // Create the filepath to save to.
    // Urp.. where do we get this from??????
    $field = $this->data['data']['filefield'];
    $file  = $this->data['data']['file'];
    $file_data = $this->file_data;
    
    //$this->object->file_data = $file_data;
    //file_put_contents('picsave', $file_data);
    
    //dsm($field);
    //dsm($file);
    
    $file['filepath'] = filefield_widget_file_path($field) . '/' . $file['filename'];

    
    // In all cases, remove the fid that came over with the old node.
    unset($file['fid']);

    // Convert title and alt data to D6 format.
    // TODO: move to filter phase!
    $file['data'] = array(
      'title' => $file['title'],
      'alt'   => $file['alt'],
    );
    $file['list'] = 1;

    // Save the incoming file.
    // Gah this is so bloody convoluted. Roll on D7.
    // ALSO! This fails if the local site has not yet saved a file to the
    // current filefield and it has a folder to itself -- because the folder
    // doesn't exist yet!!!!!!
    // ALSO ALSO! UpRPC had a ton of logic to figure out the Right Thing to do
    // if a file already existed of the same name. This should probably
    // happen here too so we don't clobber an unrelated file that 
    // happens to have the same name.
    $filepath = file_save_data($this->file_data, $file['filepath'], FILE_EXISTS_REPLACE);
    $file['filepath'] = $filepath;
    
    // Write record in files table.
    if ($this->local_exists) {
      $file['fid'] = $this->id_local;
      drupal_write_record('files', $file, array('fid'));
    }
    else {
      drupal_write_record('files', $file);      
    }
    
    //dsm($file, 'file saved');
    
    $this->id_local = $file['fid'];
    
    // Put the file back in the data... though DUH we should be using $this->entity,
    // it's what it's there for!
    $this->data['data']['file'] = $file;    
  }  
}

/**
 * Extender Class: fetch phase.
 *
 * Type:   File.
 * Source: Clients Drupal 5
 */
class TransportRemoteEntityExtender_fetch_file_d5 
  extends TransportRemoteEntityExtender_fetch_client 
  implements TransportRemoteEntityInterface_fetch {

  /**
   * Phase handler: file - fetch.
   *
   * Fetch a file from a D5 site. 
   * Because there's no file service on D5 Services, so we'd need to backport
   * the module at http://drupal.org/node/290053 and bundle it in to stuff
   * we're providing for D5.
   * In the meantime, the direct approach is fine if all the files are public, 
   * but this needs fixing if they are not: TODO.
   */
  function run_phase_fetch() {
    //dsm($this->object->source);
    
    // Figure out the base URL of the remote site based on
    $remote_url = substr($this->object->source->endpoint, 0, -15);
    
    // Retrieve the filefield data that we sneakily stashed when we came here for dependencies...
    // of course this will completely fail when we're here for upload module files
    // TODO: write a better way. See the TODO at the top of this method.
    $file = $this->object->data['data']['file'];
    
    // Note we have no timestamps coming in from D5.
    $file_url = $remote_url . '/' . $file['filepath'];

    // The URL may have spaces in and PHP is too stupid to figure this out.
    $file_url = str_replace(' ', '%20', $file_url);

    //dsm($file_url);
    $file_data = file_get_contents($file_url);
    if ($file_data === FALSE) {
      // an error happened
    }
    
    $this->object->entity = $file;
    $this->object->file_data = $file_data;
    //file_put_contents('pic', $file_data);
  }

  /**
   * Phase handler: file - filter.
   */
  function run_phase_filter() {
    // No filtering to do for files.
    // Though actually, this is where the changing of properties from D5 to D6 style should happen!
    // And obviously, run the alter hook...
  }
}

// ======================================== Extender classes: user

/**
 * Entity class for users.
 *
 * The entity class should contain methods for phases that do not have interfaces;
 * ie those that are identical no matter the source.
 */
class TransportRemoteEntity_user extends TransportRemoteEntity {
  /**
   * Phase handler: user - dependencies.
   * 
   * Users don't have dependencies -- at least in core D6.
   */  
  function run_phase_dependencies() {
    // TODO: Invoke the hook anyway as other modules may create dependencies.
  }
  
  /**
   * Phase handler: user - translate.
   */  
  function run_phase_translate() {
    // Nothing to do: no dependencies. 
    // TODO: Invoke the hook anyway as other modules may create dependencies.
  }
  
  /*
  !! TODO: need an extra phase: 'preexisting' --
  for users and files, (and terms?) check for an existing, identical entity.
  ie for user, check email
  files, filename + node
  etc
  */
  
  /**
   * Phase handler: user - save.
   */  
  function run_phase_save() {
    // user_save() is WEIRD.
    // TODO: clean up this code.
    
    $remote_user = $this->entity;
    //dsm($remote_user, 'rem user');
    
    if ($this->local_exists) {
      $local_user = user_load($this->id_local);

      unset($remote_user->uid);
      
      $user_array = (array) $remote_user;
      $new_user = user_save($local_user, $user_array);
    }
    else {
      unset($remote_user->uid);
      
      $user_array = (array) $remote_user;
      $new_user = user_save('', $user_array);
      
      $this->id_local = $new_user->uid;
      //dsm($new_user);

      // Password would have been doubly md5'ed, so set explicitly
      db_query("UPDATE {users} SET pass = '%s' WHERE uid = %d", $user_array['pass'], $new_user->uid);
    }
  }
}

/**
 * Extender Class: fetch phase.
 *
 * Type:   User.
 * Source: Clients Drupal 5
 */
class TransportRemoteEntityExtender_fetch_user_d5 
  extends TransportRemoteEntityExtender_fetch_client 
  implements TransportRemoteEntityInterface_fetch {

  /**
   * Phase handler: user - fetch.
   *
   * Fetch a user from a D5 site. 
   */
  function run_phase_fetch() {
    // Something in the Services and XMLRPC system is REALLY fussy
    // about strings vs ints, so we have to cast it.
    $uid = (int) $this->object->get_id();
    $result = $this->object->source->callMethod('user.load', $uid);
    //dpm($result, 'user 5 fetch');
    if ($this->fetch_verify_client_result($result)) {
      $this->object->entity = (object) $result;
    }
    else {
      // probably flag a problem and end the phase and return operation to the controller
    }
  }
  
  /**
   * Phase handler: user - filter.
   */
  function run_phase_filter() {
    // Run the alter hook first so modules get the raw data.
    drupal_alter('transport_preprocess_' . $this->object->type, $this->object->entity);
    
    // We don't handle roles yet, so clear them as this could be a
    // security issue.
    $this->object->entity->roles = array();
    
    // TODO: Clear/check 'theme' property.
    
    dsm($this->object->entity);
  }  
}
