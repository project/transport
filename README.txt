
Introduction
------------

Transport module aims to provide a common base on which to build services-based
applications between Drupal sites.

In plain English:
  - with Services, you can make XMLRPC calls to a remote Drupal site and get
    back data, provided you know how to pass them hashes and tokens and so on.
  - with Clients, you can just call 'node.load' with a parameter of X and get
    back data; all the complex stuff is done for you.
  - with Transport your module need only say 'get me node X', and you not only
    get the node, but the user who authored it, its attached files, taxonomy
    terms, and comments (well the last two are TODO, but that's a minor point).
    Transport does the work of calling the right methods, figuring out
    dependencies, and maintaining a record of correspondences between local and
    remote object IDs. Transport can work with multiple remote sites, and is
    agnostic of Drupal version: Transport on a Drupal 6 site can retrieve nodes
    from either a Drupal 5 or 6 site.

Some examples of applications that could be developed on top of Transport:
- migrating site content from Drupal 5 to Drupal 6
- distributing content across a network of sites
- deploying content to new sites from a template site

Requirements and stack
----------------------

Transport needs the following packages:

- On the retrieving site, that is, where content is saved:
  - clients 6.x-2.0-alpha1
  - faces 6.x-1.0-beta1
  - autoload 6.x-2.0
  
- On the distributing site, that is, where content comes from:
  - transport_services (not written yet; will be as and when extra services are
    needed ;)
  - services (currently only services 5.x-0.92, yes Drupal 5: the main drive 
    for writing this module is migration from 5 to 6; for distribution from 6 to
    6 please post a patch ;)

How it works
--------------------

There is example code to run a transport operation in the demo module.
What follows is an explanation of how it all works:

Suppose we call the beginRetrival method on our transport controller with
a single entity to retrieve:

  $entities = array('node' => array(42));
  $transport_controller->beginRetrieval($entities);

The transport controller builds itself a list of entities to work on; in this
case just a single node. It then begins work on the entity by creating a
TransportEntity object, in this case, of class TransportRemoteEntity_node.
This TransportRemoteEntity_node object then begins a process which takes it
through a number of phases:
  - fetch: the transport source is used to fetch the remote node data
  - filter: the node is cleaned up. This is where we account for changes between
    different versions of Drupal. An alter hook allows custom changes here; for
    example, if you never want to import taxonomy, or you want all incoming
    nodes to be owned by a particular user, you'd implement this hook to make
    those changes.
  - dependencies: the node is passed to a hook, allowing all modules to feed
    back a list of dependencies. For a node, that includes:
      - user module will give us the user who is the author of the node
      - taxonomy module will give us a list of term IDs on the node
      - and so on for filefield, nodereference, & co.
Here's where it gets a little weird. Having found it has a list of dependencies,
the TransportRemoteEntity_node object *stops* execution of its phases. Control
is handed back to the transport controller, which then *rearranges its list*
of entities to retrieve.
The current node is put back in the list, and in front of it go all the dependencies.
The controller moves on down its list of entities, and processes each of the
entities, which are now a user and some terms. For each of these, a remote
entity object is created, and run through all the phases (which I'll skip here
for brevity and clarity: just assumed they get saved locally).
Now when we get to the original node once again, the controller knows it's
been left mid-process, and retrieves it from the stash table as we left it.
The TransportRemoteEntity_node executes its next phase:
  - translate: All the references to other entities (in fact, the dependencies
    we just transported) are translated to local ids.
  - save: The node is saved. If this node was previously retrieved, it is
    updated.
  - record: The correspondence between the local nid and the remote nid is
    stored. This is what allows the translate phase on other entities that
    may refer to this node to know how to change their references.
  

