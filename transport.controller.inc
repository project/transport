<?php
/**
 * @file transport.controller.inc
 * Contains the transport controller class and subclasses.
 */

/**
 * 
 * The transport controller class.
 * 
 * See the transport_demo module for an example of usage.
 * 
 * Create an object of this class to run the transport process for a set of entities.
 * The transport controller needs to be given a source and a list of entities to retrieve. 
 * You can have the controller retrieve entities in small runs, to prevent timeouts. This allows the 
 * controller to be used within a BatchAPI process, or with cron. 
 * It is your responsibility to keep the controller object safe between runs, eg serialized in the database.
 * 
 * Terminology
 * 
 * - Entity: we borrow the Drupal 7 term 'entity' to mean a Drupal object such as a node, term, user, etc, but
 * extend it to mean any piece of data that can be transported; thus a file is treated as an entity
 * but is referred to as a pseudoentity (since it has no originating module).
 * - Source: The origin of the data we transport. This is represented by an object. 
 * - Set: A list of entities to retrieve, or the action of retrieving all of these.
 * One set may include several calls to the controller object's beginRetrieval() method.
 * So for example, you could call the controller in successive cron runs, and those would
 * all be part of the same set. You should however preserve the controller object in between
 * calls: destroying it ends the set.
 * You may add entities to the set list at any point.
 * A set is identified by its base time. This is set to be the request time when the controller
 * is created.
 * 
 * Technology stack
 * 
 * - Application layer: modules can use Transport to create a system for moving content around. Examples
 * include UpRPC which migrates content from old versions of Drupal to new ones, or Content Distribution
 * which synchronizes content between master and slave sites.
 * - Transport: Transport handles fetching a remote entity, including dependencies, and maintaining the relationships
 * between remote and local IDs. Transport layer is what allows the application to merely say: 'fetch nodes 2, 4, 5 from site Foo'.
 * - Clients: Clients module provides a uniform way to connect to remote services. Clients is what allows the 
 * transport layer to say 'get me the result of method node.load on site Foo'.
 * - Services: Services module runs on remote sites and provides the methods to retrieve data. Transport module adds extra services
 * to retrieve data.
 * 
 * Sources
 * 
 * The transporter system is designed to work with Client classes from the Clients module: 
 * one is provided for each version of the Services module. However, it is possible to provide
 * other types of source, since the interaction with a source is implemented in a pluggable interface.
 * Hypothetically you could have a source that just reads data from a file, for example: post a patch! ;)
 * 
 * Dependencies
 * 
 * Each remote entity discovers its dependencies, and the controller object will ensure that no entity is 
 * transported twice in one set.
 * 
 * Overview of operations:
 * - given a list of entity IDs and types
 * - create a FIFO queue
 * - create first remote entity handler and execute it
 * - the handler either completes, or returns stalled
 * - if the handler stalled, put it back in the queue and 
 *   also put all its dependencies
 * - continue processing the queue, so now we will reach the stalled
 *   handler after its dependencies have been saved.
 * 
 * This class controls the transport operation.
 * A transport controller object should be created for your current set of data to transport.
 * It will ensure that no entity is transported twice in one set.
 * 
 * Parameters:
 *   - request time (implied)
 *   - entity list
 *   - source type
 *   - source details
 *   - freshness: the length of time for a local entity to still be fresh. 
 *     Eg, if a node was last retrieved less than a day ago, do not retrieve it again
 *   - operation limit: how many operations the controller should perform before handing back
 *     control to the caller, eg batch operation, cron.
 */
class TransportController {}
class TransportControllerRetriever extends TransportController {
  // TODO: this causes the controller to stop after this many entities have
  // processed. Use for cron runs, batch jobs, etc.
  public $operation_limit;
  
  public $source_name; // The source type. TODO: maybe deduce this from the $source class?
  public $source; // An object for the source.

  // Lists of entities
  // These are keyed by the unique combination of type and id for easy lookup.
  private $shopping_list; // The list of entities still remaining to retrieve
  private $entity_processed_list; // List of entities processed so far.
  
  private $entity_tree;
  private $current_entity;
  
  // The time at which we consider the current operation to have started.
  // This is what identifies the current operation uniquely (if you're retrieving
  // from different sources in different processes... you're crazy and you should
  // submit a patch to do this differently.)
  // We guarantee that the same entity will not be retrieved twice since this time.
  private $base_time; 
  
  // Define the phases for the process. A remote entity is at any point in one
  // of these phases, and can go through all or some of them.
  // OBSOLETE
  private $phases = array(
    'init',
    'retrieve',
    'check_local',
    'filter',
    'stash',
    'dependencies',
    'requeue',
    'save',
  );
  
  /**
   * Constructor.
   *
   * @param $options
   *  An associative array with the following keys:
   *    'base_time' (optional) 
   */
  function __construct($source_name, $source, $options) {
    // Store Request time by default, or a given time (for cron runs)
    if (!isset($options['base_time'])) {
      $options['base_time'] = $_SERVER['REQUEST_TIME'];
    }
    $this->base_time = $options['base_time'];
    
    $this->source_name  = $source_name;
    $this->source       = $source;
    $this->options      = $options;
    
    // Collect information about remote entity classes.
    $this->remote_entity_classes = module_invoke_all('transport_remote_entity_info');
    //dsm($this->remote_entity_classes);    
  }
  
  /**
   * Accessor functions.
   */  
  function get_base_time() {
    return $this->base_time;
  }
  
  // TODO: Destructor:
  // Empty our stuff in the stash!!
  // -- though what do we mean by 'our'????
  
  /**
   * Entry point to begin retrieval.
   *
   * This can be called as many times as needed with new entities.
   */
  function beginRetrieval($entities) {
    // Process the list of entities we've been given and add them to our list.
    foreach ($entities as $type => $ids) {
      $class = 'TransportRemoteEntity_' . $type;
      foreach ($ids as $id) {
        // We key the shopping list by the (unique) combination of type and
        // id so we can easily check what's in it.
        $this->shopping_list[$type . '_' .$id] = array(
          'type' => $type,
          'id'   => $id,
          'class' => $class,
        );
      }
    }
    
    dsm($this->shopping_list, 'shopping list');
    
    // Work through the list.
    // The shopping_list may be altered during the loop by dependencies
    // TODO: stop the process after $process_count reaches our count limit
    // and return to the caller.
    while ($entity_data = array_shift($this->shopping_list)) {
      // Check our list of processed entities to see if we've already done this one.
      // If so, continue to the next entity.
      $type = $entity_data['type'];
      $id   = $entity_data['id'];
      if (isset($this->entity_processed_list[$type . '_' .$id])) {
        continue;
      }
      
      // Increment the process count no matter what we do in this loop iteration.
      $process_count++;

      // If the entity is stored, that is, if we already began processing it
      // and stalled the process for some reason, retrieve it from the stash.
      if (isset($entity_data['stored'])) {
        //dsm('we already did this one, retrieve it! ' . $type . '_' .$id);
        
        $result = db_query("SELECT object FROM {transport_processing_entities} WHERE base_time = %d AND type = '%s' AND id_remote = %d",
          $this->base_time, $entity_data['type'], $entity_data['id']);
        $remote_entity = unserialize(db_result($result));
        //dsm($remote_entity);
      }
      else {
        // Create a new object of the class for the wanted entity.
        $remote_entity = new $entity_data['class']($this, $entity_data);
      }
      
      dsm($remote_entity, 'remote entity');
      if (!is_object($remote_entity)) {
        dsm($remote_entity, 'Something has gone badly wrong!');
        return;
      }
      // Activate the entity handler's main process.
      $remote_entity->run();
      
      // If the entity handler is stalled, deal with its feedback.
      if ($remote_entity->stalled) {
        if ($remote_entity->phase == 'dependencies') {
          // Set the entity as stored so when we return to it we know to retrieve it.
          $entity_data['stored'] = TRUE;
        
          // Add the dependencies to the front of the queue, followed by the
          // current entity so we return to finish its processing.
          // Note that there is no need to check each dependency against the
          // list of those already processed: that will happen when the run
          // loop comes to it in its own course.
          array_unshift($this->shopping_list, $entity_data);
          array_splice($this->shopping_list, 0, 0, $remote_entity->dependencies);
          
          //dsm('stopped at deps');
          //dsm($remote_entity->dependencies);
          //dsm($this->shopping_list, 'hacked list');
        }
      }
      
      dsm("Finished run $process_count for entity: " . $entity_data['type'] . ' ' . $entity_data['id'] . ' saved as ' . $remote_entity->id_local);
            
      // Record the entity in our list of those processed.
      if ($remote_entity->complete) {
        $this->entity_processed_list[$type . '_' .$id] = $entity_data;
      }      
    } // while shopping_list
    
    //dsm($this->entity_processed_list, 'processed end');
  }
}
